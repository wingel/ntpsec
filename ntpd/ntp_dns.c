/*
 * Copyright the NTPsec project contributors
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "config.h"

#include <signal.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>

#ifdef HAVE_RES_INIT
#include <netinet/in.h>
#include <arpa/nameser.h>
#include <resolv.h>
#endif

#include "ntpd.h"
#include "ntp_dns.h"


/* Notes:

  This module also handles the start of NTS-KE.

  Only one DNS/NTS lookup active at a time

  peer->srcadr holds IPv4/IPv6/UNSPEC flag
  peer->hmode holds DNS retry time (log 2)

  Older versions of this code turned off FLAG_DNS
  so there wasn't any way to do another DNS lookup.
  Now, we turn off FLAG_DNSNTS to indicate success.

  Pool case makes new peer slots.
*/

static struct peer* active = NULL;  /* busy flag */
static pthread_t worker;
static int gai_rc;
static struct addrinfo *answer;

static void* dns_lookup(void* arg);

static int srv_response;
static unsigned char srv_buf[1024];

static bool srv_probe(struct peer *pp)
{
    char name[256];

    snprintf(name, sizeof(name), "_ntske._tcp.%s", pp->hostname);
    srv_response = res_query(name, C_IN, ns_t_srv, srv_buf, sizeof(srv_buf));

    return true;
}

static bool srv_check(struct peer *pp)
{
    unsigned found = 0;
    ns_msg msg;
    int i;

    // Turn off flag so that this function won't be called any more
    pp->cfg.flags &= (unsigned)~FLAG_NTS_SRV;

    if (srv_response >= 0) {
        ns_initparse(srv_buf, srv_response, &msg);

        // TODO sort based on priority and weight

        for (i = 0; i < ns_msg_count(msg, ns_s_an); i++) {
            ns_rr rr;
            char name[256];
            unsigned short prio;
            unsigned short weight;
            unsigned short port;
            sockaddr_u peeraddr;
            struct peer_ctl ctl;
            struct peer *peer;
            int end;

            ns_parserr(&msg, ns_s_an, i, &rr);

            if (ns_rr_type(rr) != ns_t_srv)
                continue;

            prio = ntohs(*(const unsigned short*)ns_rr_rdata(rr));
            weight = ntohs(*((const unsigned short*)ns_rr_rdata(rr) + 1));
            port = ntohs(*((const unsigned short*)ns_rr_rdata(rr) + 2));

            dn_expand(ns_msg_base(msg), ns_msg_end(msg),
                      ns_rr_rdata(rr) + 6, name, sizeof(name));

            end = strlen(name);
            // strip . at end of hostname
            if (end > 0 && name[end-1] == '.')
                end--;
            // append port number
            snprintf(name + end, sizeof(name) - end, ":%u", port);

            msyslog(LOG_INFO, "DNS: adding NTS server for %s from SRV records: %s (%u %u)",
                    pp->hostname, name, weight, prio);

            if (pp->cast_flags & MDF_POOL) {
                /* For pool, create a new NTS peer and keep the original peer */
                found++;

                memcpy(&ctl, &pp->cfg, sizeof(ctl));
                ctl.flags |= (FLAG_NTS | FLAG_LOOKUP);

                ZERO_SOCK(&peeraddr);
                peer = newpeer(&peeraddr, name, NULL, MODE_CLIENT, &ctl, MDF_UCAST, false);
                if (!peer) {
                    msyslog(LOG_INFO, "DNS: ignoring duplicate NTS server: %s", name);
                } else {
                    peer->nextdate = peer->update = peer->outdate = current_time;
                }
            } else {
                /* Otherwise, reuse the same peer, change the name, and break out of the loop */
                free(pp->hostname);
                pp->hostname = strdup(name);
                pp->cfg.flags |= (FLAG_NTS | FLAG_LOOKUP);
                pp->nextdate = pp->update = pp->outdate = current_time;
                return true;
            }
        }
    }

    if (!found) {
        // no NTS SRV records found, proceed normally
        pp->nextdate = pp->update = pp->outdate = current_time;
        return false;
    }

    dns_take_status(pp, DNS_good);

    return true;
}

/* Initially, this was only used for DNS where pp=>hostname was valid.
 * With NTS, it also gets used for numerical IP Addresses.
 */
bool dns_probe(struct peer* pp)
{
	int rc;
        sigset_t        block_mask, saved_sig_mask;
	const char	* busy = "";
	const char	*hostname = pp->hostname;

	/* Comment out the next two lines to get (much) more
	 * printout when we are busy.
	 */
        if (NULL != active)
		return false;

	if (NULL != active) {
		busy = ", busy";
	}
	if (NULL == hostname) {
		hostname = socktoa(&pp->srcadr);
	}

	msyslog(LOG_INFO, "DNS: dns_probe: %s, cast_flags:%x, flags:%x%s",
		hostname, pp->cast_flags, pp->cfg.flags, busy);
        if (NULL != active)	/* normally redundant */
		return false;

	active = pp;

        sigfillset(&block_mask);
        pthread_sigmask(SIG_BLOCK, &block_mask, &saved_sig_mask);
	rc = pthread_create(&worker, NULL, dns_lookup, pp);
        if (rc) {
	  msyslog(LOG_ERR, "DNS: dns_probe: error from pthread_create: %s, %s",
	      hostname, strerror(rc));
          pthread_sigmask(SIG_SETMASK, &saved_sig_mask, NULL);
	  return true;  /* don't try again */
	}
        pthread_sigmask(SIG_SETMASK, &saved_sig_mask, NULL);

	return true;
}

void dns_check(void)
{
	int rc;
	struct addrinfo *ai;
	const char      *hostname = active->hostname;
	DNS_Status status;

	if (NULL == hostname) {
		hostname = socktoa(&active->srcadr);
	}
	msyslog(LOG_INFO, "DNS: dns_check: processing %s, %x, %x",
		hostname, active->cast_flags, (unsigned int)active->cfg.flags);

	rc = pthread_join(worker, NULL);
	if (0 != rc) {
		msyslog(LOG_ERR, "DNS: dns_check: join failed %s", strerror(rc));
		return;  /* leaves active set */
	}

#ifndef DISABLE_NTS
	if (active->cfg.flags & FLAG_NTS_SRV) {
		srv_check(active);
		active = NULL;
		return;
	}

	if (active->cfg.flags & FLAG_NTS) {
		nts_check(active);
		active = NULL;
		return;
	}
#endif

	if (0 != gai_rc) {
		msyslog(LOG_INFO, "DNS: dns_check: DNS error: %d, %s",
			gai_rc, gai_strerror(gai_rc));
		answer = NULL;
	}

	for (ai = answer; NULL != ai; ai = ai->ai_next) {
		sockaddr_u sockaddr;
		if (sizeof(sockaddr_u) < ai->ai_addrlen)
			continue;  /* Weird */
		memcpy(&sockaddr, ai->ai_addr, ai->ai_addrlen);
		/* Both dns_take_pool and dns_take_server log something. */
		// msyslog(LOG_INFO, "DNS: Take %s=>%s",
		//		socktoa(ai->ai_addr), socktoa(&sockaddr));
		if (active->cast_flags & MDF_POOL)
			dns_take_pool(active, &sockaddr);
		else
			dns_take_server(active, &sockaddr);
	}

	switch (gai_rc) {
		case 0:
			status = DNS_good;
			break;

		case EAI_AGAIN:
			status = DNS_temp;
			break;

		/* Treat all other errors as permanent.
		 * Some values from man page weren't in headers.
		 */
		default:
			status = DNS_error;
	}

	dns_take_status(active, status);

	if (NULL != answer) {
		freeaddrinfo(answer);
	}
	active = NULL;
}

/* Beware: no calls to msyslog from here.
 * It's not thread safe.
 * This is the only other thread in ntpd.
 */
static void* dns_lookup(void* arg)
{
	struct peer *pp = (struct peer *) arg;
	struct addrinfo hints;

#ifdef HAVE_SECCOMP_H
        setup_SIGSYS_trap();      /* enable trap for this thread */
#endif

#ifdef HAVE_RES_INIT
	/* Reload DNS servers from /etc/resolv.conf in case DHCP has updated it.
	 * We only need to do this occasionally, but it's not expensive
	 * and simpler to do it every time than it is to figure out when
	 * to do it.
	 * This res_init() covers NTS too.
	 */
	res_init();
#endif

#ifndef DISABLE_NTS
	if (pp->cfg.flags & FLAG_NTS_SRV) {
		srv_probe(pp);
        }
        else
#endif
	if (pp->cfg.flags & FLAG_NTS) {
#ifndef DISABLE_NTS
		nts_probe(pp);
#endif
	} else {
		ZERO(hints);
		hints.ai_protocol = IPPROTO_UDP;
		hints.ai_socktype = SOCK_DGRAM;
		hints.ai_family = AF(&pp->srcadr);
		gai_rc = getaddrinfo(pp->hostname, NTP_PORTA, &hints, &answer);
	}

	kill(getpid(), SIGDNS);
	pthread_exit(NULL);

	/* Prevent compiler warning.
	 * More portable than an attribute or directive
	 */
	return (void *)NULL;
}

